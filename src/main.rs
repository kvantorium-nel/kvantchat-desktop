pub mod gtk_sub;

use gtk::prelude::*;
use log::info;
use log::LevelFilter as LogFilter;
use relm4::prelude::*;
use simple_logger::SimpleLogger;

struct App {
    counter: u8,
}

#[derive(Debug)]
enum AppMsg {
    LoginToAccount,
}

fn ll() {
    let a = gtk::Label::default();
    let b = gtk::Box::default();
    let pass = gtk::Entry::default();
    pass.set_icon_from_icon_name(
        gtk::EntryIconPosition::Secondary,
        Some("view-reveal-symbolic.symbolic"),
    );
    pass.set_icon_activatable(gtk::EntryIconPosition::Secondary, true);
    //pass.set_visibility(visible)
    //b.set_orientation(orientation);
    //a.set_css_classes("classes")
    pass.connect_icon_press(|ds, ii| {
        if ds.icon_name(ii).unwrap() == gtk_sub::icon_name::VIEW_REVEAL_SYMBOLIC {
            ds.set_icon_from_icon_name(
                gtk::EntryIconPosition::Secondary,
                Some(gtk_sub::icon_name::VIEW_REVEAL_SYMBOLIC),
            );
            ds.set_icon_activatable(gtk::EntryIconPosition::Secondary, true);
        } else if ds.icon_name(ii).unwrap() == gtk_sub::icon_name::VIEW_CONCEAL_SYMBOLIC {
            ds.set_icon_from_icon_name(
                gtk::EntryIconPosition::Secondary,
                Some(gtk_sub::icon_name::VIEW_CONCEAL_SYMBOLIC),
            );
            ds.set_icon_activatable(gtk::EntryIconPosition::Secondary, false);
        }
    });

    //let grid = gtk::Grid::new();
    // grid.riw
    b.set_baseline_position(gtk::BaselinePosition::Center);
}

#[relm4::component]
impl SimpleComponent for App {
    type Init = u8;
    type Input = AppMsg;
    type Output = ();
    type Widgets = AppWidgets;

    view! {
        ss = gtk::Window {
            set_title: Some("Kvantchat: Desktop"),
            set_default_size: (400, 500),

            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_spacing: 5,
                set_margin_all: 30,
                set_baseline_position: gtk::BaselinePosition::Center,

                gtk::Label {
                    set_label: "Login",
                    set_margin_all: 5,
                    inline_css: "font-size: 60px; font-weight: bold;"
                },

                gtk::Box {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_baseline_position: gtk::BaselinePosition::Top,
                    //set_spacing: 5,

                    gtk::Label {
                        set_label: "email: ",
                        inline_css: "font-size: 20px",
                    },

                    gtk::Entry {
                        set_tooltip_text: Some("test@gmail.com"),
                        inline_css: "font-size: 20px",
                    },
                },

                gtk::Box {
                    set_orientation: gtk::Orientation::Horizontal,
                    set_spacing: 5,

                    gtk::Label {
                        set_label: "password: ",
                        inline_css: "font-size: 20px",
                    },

                    gtk::Entry {
                        set_tooltip_text: Some("SuperMegaPasswordSAKn2343@"),
                        inline_css: "font-size: 20px",
                        set_visibility: false,
                        set_icon_from_icon_name: (gtk::EntryIconPosition::Secondary, Some("view-reveal-symbolic")),
                        set_icon_activatable: (gtk::EntryIconPosition::Secondary, true),
                        connect_icon_press => |password, password_icon_position| {
                            gtk_sub::template::view_password_symbolic(password, &password_icon_position);
                        },
                    },
                },

                gtk::Button {
                    set_label: "Login",
                    inline_css: "font-size: 40px; font-weight: bold; border-radius: 12px;",
                    set_margin_all: 20,
                    connect_clicked[sender] => move |_| {
                        sender.oneshot_command(async move {
                            AppMsg::LoginToAccount
                        })
                    },
                },

                gtk::Button {
                    set_label: "Registration",
                    set_margin_all: 20,
                    inline_css: "border-radius: 12px;"
                }
            }
        }
    }

    // Initialize the component.
    fn init(
        counter: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = App { counter };

        // Insert the code generation of the view! macro here
        let widgets = view_output!();

        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, _sender: ComponentSender<Self>) {
        match msg {
            AppMsg::LoginToAccount => {
                info!("Login form: AppMsg::LoginToAccount running");
                let aa = kvantchat_api::KvantChatClient::connect("3");
                info!("Login form: AppMsg::LoginToAccount done");
            }
        }
    }
}

fn main() {
    SimpleLogger::default()
        .with_level(LogFilter::Error)
        .with_module_level("kvantchat_desktop", LogFilter::Info)
        .init()
        .unwrap();

    let app = RelmApp::new("relm4.example.simple");
    app.run::<App>(0);
}
