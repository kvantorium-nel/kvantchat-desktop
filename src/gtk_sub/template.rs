use crate::gtk_sub::icon_name;
use gtk::prelude::*;
use relm4::prelude::*;

pub fn view_password_symbolic(
    password_entry: &gtk::Entry,
    password_entry_icon_position: &gtk::EntryIconPosition,
) {
    let icon_name = password_entry
        .icon_name(*password_entry_icon_position)
        .unwrap();

    if icon_name == icon_name::VIEW_REVEAL_SYMBOLIC {
        password_entry.set_icon_from_icon_name(
            gtk::EntryIconPosition::Secondary,
            Some(icon_name::VIEW_CONCEAL_SYMBOLIC),
        );
        password_entry.set_visibility(true);
    } else if icon_name == icon_name::VIEW_CONCEAL_SYMBOLIC {
        password_entry.set_icon_from_icon_name(
            gtk::EntryIconPosition::Secondary,
            Some(icon_name::VIEW_REVEAL_SYMBOLIC),
        );
        password_entry.set_visibility(false);
    }
}
