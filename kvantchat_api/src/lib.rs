use anyhow::Result;

/// Импорт нашего gRPC через tonic
pub mod kvantchat_tonic {
    tonic::include_proto!("kvantchat");
}

use kvantchat_tonic::kvantchat_protocol_client::KvantchatProtocolClient;
use kvantchat_tonic::{LoginReplay, LoginRequest, RegistrationReplay, RegistrationRequest};
use tonic::transport::Channel;

pub struct KvantChatClient {
    client: KvantchatProtocolClient<Channel>,
}

impl KvantChatClient {
    pub async fn connect(address: &str) -> Result<Self> {
        let connect: KvantchatProtocolClient<_> =
        KvantchatProtocolClient::connect(address.to_string()).await?;

        Ok(Self {
            client: connect.into(),
        })
    }

    pub async fn login_to_account(&mut self, login: String, password: String) -> Result<LoginReplay> {
        let request = tonic::Request::new(LoginRequest {
            login,
            password
        });

        let result = self.client.login(request).await?;
        Ok((*result.get_ref()).clone())
    }

    pub async fn registration(&mut self, login: String, password: String) -> Result<RegistrationReplay> {
        let request = tonic::Request::new(RegistrationRequest {
            login,
            password
        });

        let result = self.client.registration(request).await?;
        Ok((*result.get_ref()).clone())
    }
}