fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("../kvantchat-server/proto/kvantchat.proto")?;
    Ok(())
}
